
import { EStore } from '../../models/Store';
import { getConnection } from 'typeorm';

export class MQuery {


    public queryProductDepositInStore = async () => {
        return await getConnection().getRepository(EStore).find({
            where: {
                end_date: ""
            }
        })
    }

    public queryProductNotInStore = async () => {
        return await getConnection().query(`select * from e_store where end_date != "";`)
    }

    public resultAmount = async (): Promise<number> => {
        const result: Array<EStore> = await this.queryProductNotInStore()
        let total = 0;
        result.forEach(r => {
            total += r.amount
        });
        return total

    }

}