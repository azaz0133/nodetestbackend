import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { EStore } from './Store';



@Entity()
export class EUser {

    @PrimaryGeneratedColumn()
    id? : number

    @Column({
        unique: true
    })
    username? : string

    @Column()
    password? : string

    @Column()
    isAdmin? : boolean

    @OneToMany(type => EStore, store => store.user)
    belongs? : EStore[]

    @CreateDateColumn()
    created_at? : Date

    @UpdateDateColumn()
    updated_at? : Date
}