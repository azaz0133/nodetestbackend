import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, OneToOne, JoinColumn, ManyToOne } from "typeorm";
import { EProduct } from './Product';
import { EUser } from './User';



@Entity()
export class EStore {

    @PrimaryGeneratedColumn()
    id? : number

    @Column()
    start_date? : string

    @Column()
    end_date? : string

    @Column()
    amount? : number

    @OneToOne(type => EProduct)
    @JoinColumn()
    product? : EProduct

    @ManyToOne(type => EUser,user => user.belongs)
    @JoinColumn()
    user? : EUser

    @CreateDateColumn()
    created_at? : Date

    @UpdateDateColumn()
    updated_at? : Date

}