import { IPayloadRegisterUser } from '../../interfaces/payloads';
import { CommonModel } from '../../utils/common_model';
import { EUser } from '../../models/User';
import * as bcrypt from 'bcryptjs'
import { getConnection } from 'typeorm';

export class MUser {

    model: CommonModel<EUser>

    constructor() {
        this.model = new CommonModel(EUser, ["belongs"])
    }


    public register = async (attr: IPayloadRegisterUser): Promise<void> => {
        try {

            await this.model.create({
                username: attr.username,
                isAdmin: false,
                password: this.encryptPassword(attr.password)
            })
            return;
        } catch (error) {
            throw new Error(error)
        }
    }

    public findByUsername = (username: string): Promise<EUser> => new Promise(async (resolve, reject) => {
        const user = await getConnection().getRepository(EUser).findOne({
            where: {
                username
            }
        })
        if (!user) {
            reject({
                message: "not found"
            })
        }
        resolve(user)
    })


    private encryptPassword = (password: string): string => bcrypt.hashSync(password, 12)


}