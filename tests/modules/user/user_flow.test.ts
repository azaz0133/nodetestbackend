
import { expect } from 'chai';
import Axios from 'axios'
import {API_TEST} from '../../constant'
import * as Faker from "Faker"



describe("Test Flow User", () => {
const username = Faker.Name.firstName()
    it("register user test", async () => {

        const response = await Axios.post(API_TEST + "/user/register", {
            username,
            password: "test"
        })

        expect(response.status,"statusCode sholud equal 201").to.equal(201)

        expect(response.data.status,"success should return status created").to.equal("created")

    })

    it("login user test",async () => {
        const response = await Axios.post(API_TEST + "/session/login", {
            username,
            password: "test"
        })

        expect(response.status,"statusCode sholud equal 201").to.equal(201)

        expect(response.headers,"should have Authorization header").to.have.property("authorization")
    })

})