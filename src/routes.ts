import { IRoute } from './interfaces/express';
import { rUser } from './modules/user/routes';
import { rDeposit } from './modules/deposit/routes';
import { rSession } from './modules/session/routes';
import { rQuery } from './modules/queryInfo/routes';




export function router(router: IRoute) {
    rUser(router)
    rDeposit(router)
    rSession(router)
    rQuery(router)
    return router
}