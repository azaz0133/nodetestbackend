import { Server } from './server';
import 'reflect-metadata'
import { createConnection, getConnection } from 'typeorm';
import { configDB } from './utils/db';
import { EProductType } from './models/Product_type';
import { CommonModel } from './utils/common_model';
import dotenv = require('dotenv')

dotenv.config()

// สร้าง ข้อมูล type เริ่มต้น หากยังไม่มี
async function initProductType() {
    for (let i = 1; i < 4; i++) {
        const type = await getConnection().getRepository(EProductType).findOne({
            where: {
                id: i
            }
        })
        if (!type) {
            const typeDetail = i == 1 ? {
                name: "อาหารเสริม",
                price: 1,
            } : i == 2 ? {
                name: "เสื้อผ้า",
                price: 20
            } : {
                        name: "อื่นๆ",
                        price: 10
                    }
            let createDetail = {
                type: typeDetail.name,
                id: i,
                price: typeDetail.price,
                time_increment_perday: 0
            }
            if (i == 1) {
                createDetail.time_increment_perday = 2
            }
                new CommonModel<EProductType>(EProductType, ["products"]).create(createDetail)
        }
    }
}


// เริ่มการทำงาน
export const startApp = async () => {
    const PORT = process.env.PORT || 8080
    try {
        // ต่อ database
        await createConnection(configDB)
        // เพิ่ม type ถ้ายังไม่มี
        initProductType()

        // เริ่ม server
        new Server().initServer().listen(PORT, () => {
            console.log(`Server running on port  ` + PORT);
        })
    } catch (error) {
        console.log(error);
        process.exit()
    }
}


startApp()