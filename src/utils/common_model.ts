import { getConnection } from 'typeorm';

// logic ที่ใช้ซ้ำ
export class CommonModel<T> {

    entity: any
    relations: Array<string>

    constructor(entity, relations) {
        this.entity = entity;
        this.relations = relations
    }

    public create = (attr: T): Promise<any> => new Promise(async (resolve, reject) => {
        try {
            const result = await getConnection().createQueryBuilder().insert().into(this.entity).values([{
                ...attr
            }]).execute()
            resolve(result.identifiers[0].id)
        } catch (error) {
            reject(error)
        }
    })

    public update = (id: number, attr: T): Promise<{
        updated: boolean,
        error?: Error
    }> => new Promise(async (resolve, reject) => {
        try {
            await getConnection().createQueryBuilder().update(this.entity).set({
                ...attr
            }).where("id = :id",{
                id
            }).execute()
            resolve({
                updated: true
            })
        } catch (error) {
            reject({
                updated: false,
                error
            })
        }
    })

    public delete = (id: number): Promise<{
        deleted: boolean
    }> => new Promise(async (resolve, reject) => {
        try {
            await getConnection().createQueryBuilder().delete().from(this.entity).where("id = :id", {
                id
            }).execute()
            resolve({
                deleted: true
            })
        } catch (error) {
            reject({
                deleted: false,
                error
            })
        }
    })

    public findAll = (): Promise<Array<T>> => new Promise(async (resolve, reject) => {
        try {
            const results = await getConnection().getRepository(this.entity).find({
                relations: this.relations
            })
            resolve(results as Array<T>)
        } catch (error) {
            reject(error)
        }
    })

    public findById = (id: number,relations?: Array<string>): Promise<T> => new Promise(async (resolve, reject) => {
        try {
            const result = await getConnection().getRepository(this.entity).findOne({
                where: {
                    id
                },
                relations: relations || this.relations
            })
            resolve(result as T)
        } catch (error) {
            console.log(error);
            reject(error)
        }
    })


}