# Nodejs Testing By azaz0133


# ER-DIAGRAM
![alt text](https://gitlab.com/azaz0133/nodetestbackend/raw/master/doc/er.png)

# How To Start

environment variable 
 - SECRET_KEY ใช้ในการออก โทเคน
 - PORT รันที่ไหน
 - HOST_DATABASE 
 - PORT_DATABASE
 - DATABASE ชื่อดาต้าเบส


 1. yarn install
 2. yarn run migration
 3. yarn run migration:run
 4. yarn run dev 

# Api

ลิ้งค์ ทดสอบ api --> /api-docs

 1.  /api/user/register --> ใช้สมัคร
 2. /api/session/login --> ใช้ login เพื่อเอา token จาก header
 3. /api/store/deposit --> ฝากของ ต้องเอา
token ใส่ไปใน header authorization * 
typeId มี 1 2 3  1 คือ อาหารเสริม 2 เสื้อผ้า 3 อื่น 
ในกรณี เลือก type 2 ไม่ต้องใส่ height width length ก็ได้
4. /api/store/withdraw --> นำของออก ต้องเอา
token ใส่ไปใน header authorization *
5. /api/store/query/in --> ดูของในโกดัง
6. /api/store/query/out --> ดูของที่นำออกแล้ว
7. /api/store/query/amount --> ดูรายได้ทั้งหมดที่รับ

## Tools

 1. Typescript
 2. Nodejs
 3. mysql
 4. typeorm
 5. Docker