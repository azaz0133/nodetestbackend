import { expect } from 'chai';
import { SnGeoMetric } from '../../src/utils/math';



describe("Test Math function", () => {
    describe("Test Geometric Sequence", () => {
        it("a1 = 1 r = 2 n = 8 must equal 255", () => {
            expect(SnGeoMetric(1, 2, 8)).equal(255)
        })

        it("a1 = 1 r = 3 n = 5 must equal 121", () => {
            expect(SnGeoMetric(1, 3, 5)).equal(121)
        })

        it("r = 1 should error", () => {
            expect(() => SnGeoMetric(1, 1, 1)).to.throw("r not equal 1")
        })
    })
})