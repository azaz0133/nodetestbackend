import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { EProduct } from './Product';



@Entity()
export class EProductType {

    @PrimaryGeneratedColumn()
    id? : number

    @Column()
    type? : string

    @Column()
    price? : number

    @Column()
    time_increment_perday? : number

    @OneToMany(type => EProduct, product => product.id)
    products? : EProduct[]

    @CreateDateColumn()
    created_at? : Date

    @UpdateDateColumn()
    updated_at? : Date

}