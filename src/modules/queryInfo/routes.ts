
import { IRoute } from '../../interfaces/express';
import { CQuery } from './controller';

export function rQuery(router: IRoute) {
    const controller = new CQuery()
    router.get("/store/query/in", controller.getProductInStore)
    router.get("/store/query/out", controller.getProductOutStore)
    router.get("/store/query/amount", controller.getAllAmount)

}