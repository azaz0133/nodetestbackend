import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import { router } from './routes';
import swagger from 'swagger-ui-express'
import doc from '../api_docs.json'

export class Server {

    app: express.Application

    constructor() {
        this.app = express()
    }

    public initServer = (): express.Application => {

        this.app.use(cors({
            origin: "*"
        }))

        this.installMiddlewares()

        this.app.use("/api", router(express.Router({
            caseSensitive: true
        })))

        return this.app;
    }

    private installMiddlewares = (): void => {
        this.app.use(cors({
            origin: "*",
            exposedHeaders: ["Authorization"]
        }))
        this.app.use(
            bodyParser.urlencoded({
                extended: false,
            }),
        )
        this.app.use("/api-docs", swagger.serve, swagger.setup(doc))
        this.app.use(bodyParser.json())
    }
}



