import moment from 'moment'
import { DATE_FORMAT } from '../constants';

// function หา อนุกรมเรขาคณิต ใช้ตอน อาหารเสริม
export const SnGeoMetric = (
    a1: number,
    r: number,
    n: number
): number => {
    if (r == 1) {
        throw new Error("r not equal 1")
    }
    return a1 * (1 - Math.pow(r, n)) / (1 - r)
}
// แปลงหน่วย ลบ.ซม. เป็น ลบ.ม.
export const cubicCentiMeterToCubicMetre = (num: number) => num / 1000000

// แปลง ปริมตารเป็นน้ำหนัก จากสูตร D = M/V
export const convertVolumeToMass = (volume: number) => cubicCentiMeterToCubicMetre(volume) * 100 // กำหนดให้ความหนาแน่นวัตถุ ทุกชิ้นในจักรวาลนี้เท่ากับ 100 กิโลกรัมต่อลูกบาศก์เมตร

// หาระยะเวลาฝากของ เศษปัดขึ้นเพิ่มเป็น 1 วัน
export const diffTime = (timeStart,timeEnd) => Math.ceil(moment.duration(timeEnd.diff(moment(timeStart, DATE_FORMAT))).asDays())