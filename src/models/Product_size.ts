import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";


@Entity()
export class EProductSize {

    @PrimaryGeneratedColumn()
    id?: number

    @Column()
    height?: number

    @Column()
    width?: number

    @Column()
    length?: number

    @CreateDateColumn()
    created_at?: Date

    @UpdateDateColumn()
    updated_at?: Date
}