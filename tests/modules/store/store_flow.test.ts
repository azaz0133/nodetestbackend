import Axios from 'axios'
import { expect } from 'chai';
import { API_TEST } from '../../constant';
import { SnGeoMetric, cubicCentiMeterToCubicMetre } from '../../../src/utils/math';
import { convertVolumeToMass } from '../../../src/utils/math';


// SUPPLEMENTARY_FOOD_ID
// SHIRTS_ID
// OTHER_ID
const TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjgwLCJpYXQiOjE1NTcwMzc1NDcsImV4cCI6MTU2OTk5NzU0N30.dTEhSjVo1g0VYr-nA9LlITkQijmSho2YXJIFProvMLY"
describe("Test store flow", () => {

    describe("deposit and withdraw", () => {
        beforeAll(() => {

        })
        describe("SUPPLEMENTARY", () => {
            let storeIdSUPPLEMENTARY = null
            it("deposit SUPPLEMENTARY", async () => {
                const response = await Axios.post(API_TEST + "/store/deposit", {
                    userId: 89,
                    size: {
                        height: 120,
                        width: 100,
                        length: 100
                    },
                    typeId: 1,
                    name: "test product2"
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })
                storeIdSUPPLEMENTARY = response.data.storeId
                expect(response.data, "status equal created").to.deep.property("status").equal("created")
                expect(response.data, "must have store id").to.have.property("storeId")

            })

            it("withdraw SUPPLEMENTARY 2 day", async () => {
                const response = await Axios.post(API_TEST + "/store/withdraw", {
                    userId: 89,
                    storeId: storeIdSUPPLEMENTARY,
                    fakerDateWithdraw: "2019-05-06 12:20",
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })

                expect(response.data.amount).to.equal(120 * 100 * 100 * SnGeoMetric(1, 2, 2))
            })

            it("withdraw SUPPLEMENTARY A MINUTE AGO", async () => {
                const response = await Axios.post(API_TEST + "/store/withdraw", {
                    userId: 89,
                    storeId: storeIdSUPPLEMENTARY,
                    fakerDateWithdraw: "2019-05-05 13:50",
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })

                expect(response.data.amount).to.equal(120 * 100 * 100 * SnGeoMetric(1, 2, 1))
            })

        })

        describe("SHIRT", () => {
            let storeIdSHIRT = null
            it("deposit SHIRTS", async () => {
                const response = await Axios.post(API_TEST + "/store/deposit", {
                    userId: 89,
                    size: {
                        height: 120,
                        width: 100,
                        length: 100
                    },
                    typeId: 2,
                    name: "test product3"
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })
                storeIdSHIRT = response.data.storeId
                expect(response.data, "status equal created").to.deep.property("status").equal("created")
                expect(response.data, "must have store id").to.have.property("storeId")
            })

            it("withdraw SHIRTS", async () => {
                const response = await Axios.post(API_TEST + "/store/withdraw", {
                    userId: 89,
                    storeId: storeIdSHIRT,
                    fakerDateWithdraw: "2019-05-09 12:50",
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })

                expect(response.data.amount, "amount shold equal 120*100*100*2").to.equal(20 * convertVolumeToMass(120 * 100 * 100) * 5)
            })

            it("withdraw SHIRTS with no size or volume", async () => {

                const responseCreate = await Axios.post(API_TEST + "/store/deposit", {
                    userId: 89,
                    // size: {
                    //     height: 120,
                    //     width: 100,
                    //     length: 100
                    // },
                    typeId: 2,
                    name: "test product3"
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })
                expect(responseCreate.data, "status equal created").to.deep.property("status").equal("created")
                expect(responseCreate.data, "must have store id").to.have.property("storeId")
                const response = await Axios.post(API_TEST + "/store/withdraw", {
                    userId: 89,
                    storeId: responseCreate.data.storeId,
                    fakerDateWithdraw: "2019-05-06 13:50",
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })

                expect(response.data.amount, "amount shold equal 50").to.equal(50*1)
            })
        })

        describe("OTHER", () => {
            let storeId = null
            it("deposit other type ", async () => {
                const response = await Axios.post(API_TEST + "/store/deposit", {
                    userId: 89,
                    size: {
                        height: 120,
                        width: 100,
                        length: 100
                    },
                    typeId: 3,
                    name: "test product4"
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })
                storeId = response.data.storeId
            })
            it("withdraw other type ", async () => {
                const response = await Axios.post(API_TEST + "/store/withdraw", {
                    userId: 89,
                    storeId: storeId,
                    fakerDateWithdraw: "2019-05-06 12:50",
                }, {
                        headers: {
                            authorization: TOKEN
                        }
                    })

                expect(response.data.amount).to.equal(cubicCentiMeterToCubicMetre(120 * 100 * 100) * 10 * 2)
            })
        })
    })

})