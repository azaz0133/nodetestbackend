import { DATE_FORMAT } from './../../constants';
import { CommonModel } from '../../utils/common_model';
import { EProduct } from '../../models/Product';
import { EProductSize } from '../../models/Product_size';
import { EProductType } from '../../models/Product_type';
import { IPayloadCreateProduct, IPayloadWithdrawProduct } from '../../interfaces/payloads';
import { EUser } from '../../models/User';
import { EStore } from '../../models/Store';
import { SUPPLEMENTARY_FOOD_ID, SHIRTS_ID, OTHER_ID } from '../../constants';
import moment = require('moment');
import { SnGeoMetric, convertVolumeToMass, cubicCentiMeterToCubicMetre, diffTime } from '../../utils/math';



export class MDeposit {

    productModel: CommonModel<EProduct>;
    productTypeModel: CommonModel<EProductType>;
    productSizeModel: CommonModel<EProductSize>;
    userModel: CommonModel<EUser>;
    storeModel: CommonModel<EStore>;

    constructor() {
        this.productModel = new CommonModel(EProduct, [])
        this.productTypeModel = new CommonModel(EProductType, [])
        this.productSizeModel = new CommonModel(EProductSize, [])
        this.userModel = new CommonModel(EUser, [])
        this.storeModel = new CommonModel(EStore, [])
    }

    public createDeposit = async (attr: IPayloadCreateProduct): Promise<number> => {
        try {
            const user = this.userModel.findById(attr.userId)
            const typeProduct = this.productTypeModel.findById(attr.typeId)
            const successFinds = await Promise.all([user, typeProduct])
            let idSize = null
            if (attr.height && attr.width && attr.length) {
                idSize = await this.productSizeModel.create({
                    height: attr.height,
                    width: attr.width,
                    length: attr.length
                })
            }
            else {
                idSize = await this.productSizeModel.create({
                    height: 0,
                    width: 0,
                    length: 0
                })
            }
            const sizeProduct = await this.productSizeModel.findById(idSize)
            const idProduct = await this.productModel.create({
                name: attr.name,
                size: sizeProduct,
                product_type: successFinds[1],
            })
            const product = await this.productModel.findById(idProduct)

            const id = await this.storeModel.create({
                product,
                user: successFinds[0],
                start_date: moment(new Date()).format(DATE_FORMAT),
            })
            return id

        } catch (error) {
            console.log(error);
            throw new Error(error.message)
        }
    }

    public withdrawProduct = async (
        attr: IPayloadWithdrawProduct,
    ): Promise<number> => {
        try {
            const user = this.userModel.findById(attr.userId, ["belongs"])
            const storeProduct = this.storeModel.findById(attr.storeId, ["product", "user"])
            const findSucces = await Promise.all([user, storeProduct])
            if (findSucces[0].id != findSucces[1].user.id) {
                throw new Error("this store not belonging you")
            }

            const { size, product_type } = await this.productModel.findById(findSucces[1].product.id, ["size", "product_type"])
            const volume = size.length * size.width * size.height
            let toDay = moment(moment(new Date()).format(DATE_FORMAT), DATE_FORMAT)
            if (attr.fakerDateWithdraw) {
                toDay = moment(moment(attr.fakerDateWithdraw).format(DATE_FORMAT), DATE_FORMAT)
            }

            const amount = this.calculatePrice(
                product_type.id,
                product_type.price,
                product_type.time_increment_perday,
                findSucces[1].start_date,
                toDay,
                volume)

            await this.storeModel.update(attr.storeId, {
                amount,
                end_date: attr.fakerDateWithdraw || moment(new Date()).format(DATE_FORMAT)
            })

            return amount

        } catch (error) {
            throw new Error(error)
        }
    }

    // คำนวณของ
    public calculatePrice = (
        type: number,
        priceType: number,
        incrementPerday: number,
        dateDeposit: string,
        withdrawDate: moment.Moment,
        volume: number
    ): number => {

        let lengthDepositDay = diffTime(dateDeposit, withdrawDate)
        if (lengthDepositDay == 0) {
            lengthDepositDay = 1
        }
        switch (type) {
            case SUPPLEMENTARY_FOOD_ID: // อาหารเสริม
                return volume * priceType * SnGeoMetric(1, incrementPerday, lengthDepositDay)
            case SHIRTS_ID: // เสื้อผ้า
                if (volume == 0) {
                    return 50 * lengthDepositDay
                }
                return priceType * convertVolumeToMass(volume) * lengthDepositDay
            case OTHER_ID: //อื่นๆ
                return cubicCentiMeterToCubicMetre(volume) * priceType * lengthDepositDay
            default: return 0
        }
    }



}