import {
    ConnectionOptions
} from 'typeorm'

// config db
export const configDB: ConnectionOptions = {
    host: process.env.HOST_DATABASE || "localhost",
    port: parseInt(process.env.PORT_DATABASE) || 3306,
    type: "mysql",
    username: "root",
    password: "",
    database: process.env.DATABASE || "deposit_store",
    logger: "debug",
    synchronize: false,
    logging: true,
    entities: [
        "src/models/**/*.ts"
    ],
    migrations: [
        "src/migration/**/*.ts"
    ],
    cli: {
        entitiesDir: "src/models",
        migrationsDir: "src/migration",
        subscribersDir: "src/subscriber"
    }

}