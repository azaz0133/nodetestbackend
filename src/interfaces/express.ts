import * as express from 'express'

export interface IRequest extends express.Request {

}

export interface IResponse extends express.Response {

}

export interface IRoute extends express.Router {

}

export interface INext extends express.NextFunction {
    
}
