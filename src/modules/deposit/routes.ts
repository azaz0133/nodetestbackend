
import { IRoute } from '../../interfaces/express';
import { CDeposit } from './controller';
import { authorize } from '../../middleware/authorize';


export function rDeposit(router: IRoute): void {
    const controller = new CDeposit()
    router.post("/store/deposit",authorize, controller.addDepositProductToStore)
    router.post('/store/withdraw', controller.withdrawProduct)
}