import * as jwt from 'jsonwebtoken'
import { MUser } from '../user/model';
import * as bcrypt from 'bcryptjs'
import { IRequest, IResponse } from '../../interfaces/express';

export class CSession {
    userModel: MUser

    constructor() {
        this.userModel = new MUser()
    }

    public login = async (
        req: IRequest,
        res: IResponse
    ): Promise<void> => {
        const { username, password } = req.body

        if (!username || !password) {
            res.status(400).json({
                status: "failure",
                message: "login need username and password"
            })
        }
        try {

            const user = await this.userModel.findByUsername(username)

            if (bcrypt.compareSync(password, user.password)) {
                const token = jwt.sign({
                    userId: user.id,
                    username: user.username
                }, process.env.SECRET_KEY || "test", {
                        expiresIn: "1h"
                    })
                res.header("Authorization", "Bearer " + token)
                res.status(201)
                res.json({
                    message:"Login already"
                })
                return
            } 
            res.status(400).json({
                status:"failure",
                message:"wrong password"
            })
            console.log("object");
            return
        } catch (error) {
            console.log(error);
            res.status(500).json({
                message:error.message
            })
        }
    }
}