import { MDeposit } from './../../src/modules/deposit/model';
import { DATE_FORMAT } from './../../src/constants';
import { expect } from 'chai'
import moment = require('moment');



describe("Store Business Logic", () => {
    const model = new MDeposit()
    describe("Calculating TypeProduct SUPPLEMENTARY_FOOD_ID", () => {

        it("deposit date = 2019-10-10 withdraw date = 2019-10-18 and volume = 1000 cm^3 should equl 255000", () => {
            const result = model.calculatePrice(1, 1, 2, "2019-10-10", moment("2019-10-18", DATE_FORMAT), 1000)
            expect(result).equal(255000)
        })

        it("deposit date = 2019-10-10 withdraw date = 2019-10-20 and volume = 5000 cm^3 should equl 5115000", () => {
            const result = model.calculatePrice(1, 1, 2, "2019-10-10", moment("2019-10-20", DATE_FORMAT), 5000)
            expect(result).equal(5115000)
        })

    })

    describe("Calculating TypeProduct SHIRTS_ID",() =>{
        it("deposit date = 2019-10-10 withdraw date = 2019-10-18 and volume = 1000 cm^3 should equl 16", () => {
            const result = model.calculatePrice(2, 20, null, "2019-10-10", moment("2019-10-18", DATE_FORMAT), 1000)
            expect(result).equal(16)
        })

        it("deposit date = 2019-10-10 withdraw date = 2019-10-20 and volume = no mass should equl 500", () => {
            const result = model.calculatePrice(2, 1, 2, "2019-10-10", moment("2019-10-20", DATE_FORMAT), 0)
            expect(result).equal(500)
        })
    })

    describe("Calculating TypeProduct OTHER_ID",() =>{
        it("deposit date = 2019-10-10 withdraw date = 2019-10-18 and volume = 1000 cm^3 should equl 0.008", () => {
            const result = model.calculatePrice(3, 10, null, "2019-10-10", moment("2019-10-18", DATE_FORMAT), 1000)
            expect(result).equal(0.08)
        })

    })

})