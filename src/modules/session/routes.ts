
import { IRoute } from '../../interfaces/express';
import { CSession } from './controller';


export function rSession(router: IRoute) {
    const controller = new CSession()
    router.post("/session/login", controller.login)
}