
import { IRoute } from '../../interfaces/express';
import { CUser } from './controller';


export function rUser(router: IRoute) {
    const controller = new CUser()

    router.post("/user/register", controller.getRegister)
}