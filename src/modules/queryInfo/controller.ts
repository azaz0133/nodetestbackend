
import { MQuery } from './model';
import { IRequest, IResponse } from '../../interfaces/express';



export class CQuery {

    model: MQuery

    constructor(){
        this.model = new MQuery()
    }

    getProductInStore = async (
        req:IRequest,
        res:IResponse
    ): Promise<void> => {
        try {
            const results = await this.model.queryProductDepositInStore()
            res.status(200).json({
                status:"success",
                message: "get product in store",
                results
            })
        } catch (error) {
            res.status(400).json({
                status:"failure"
            })
        }
    }

    getProductOutStore = async (
        req:IRequest,
        res:IResponse
    ): Promise<void> => {
        try {
            const results = await this.model.queryProductNotInStore()
            res.status(200).json({
                status:"success",
                message: "get product not in store",
                results
            })
        } catch (error) {
            res.status(400).json({
                status:"failure"
            })
        }
    }

    getAllAmount = async (
        req:IRequest,
        res:IResponse
    ): Promise<void> => {
        try {
            const amount = await this.model.resultAmount()
            res.status(200).json({
                status:"success",
                message: "get all amount receive",
                amount
            })
        } catch (error) {
            res.status(400).json({
                status:"failure"
            })
        }
    }
}