

export interface IPayloadCreateProduct {
    name: string
    height: number
    width: number
    length: number
    typeId: number
    userId?: number,
    auth?: number
}

export interface IPayloadCreateProductType {
    amount: number
    type: string
    time_increment_perday: number

}

export interface IPayloadWithdrawProduct {
    userId: number
    storeId: number
    fakerDateWithdraw?: string
}

export interface IPayloadRegisterUser {
    username: string
    password: string
}