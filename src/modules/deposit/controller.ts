import { IRequest, IResponse } from "../../interfaces/express";
import { MDeposit } from './model';
import { verifyToken } from '../../middleware/authorize';


export class CDeposit {

    model: MDeposit

    constructor() {
        this.model = new MDeposit()
    }
    // เพิ่มฝากของ
    public addDepositProductToStore = async (
        req: IRequest,
        res: IResponse
    ): Promise<void> => {
        if (req.headers.authorization) {
            const userPayload = verifyToken(req.headers.authorization)
            req.body.userId = userPayload.userId
        }
        try {
            const storeId = await this.model.createDeposit(req.body)
            res.status(201).json({
                status: "created",
                message: "success deposit",
                storeId
            })
        } catch (error) {
            res.status(500).json({
                status: "failure",
                message: "something wrong : " + error.message
            })
        }
    }
    // เอาของออก
    public withdrawProduct = async (
        req: IRequest,
        res: IResponse
    ): Promise<void> => {
        if (req.headers.authorization) {
            const userPayload = verifyToken(req.headers.authorization)
            req.body.userId = userPayload.userId
        }
        try {
            const amount = await this.model.withdrawProduct(req.body)
            res.status(200).json({
                status: "ok",
                message: "withdraw product already",
                amount
            })
        } catch (error) {
            console.log(error);
            res.status(500).json({
                status: "failure",
                message: error.message
            })
        }
    }

}