import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToOne, JoinColumn } from 'typeorm'
import { EProductType } from './Product_type';
import { EProductSize } from './Product_size';

@Entity()
export class EProduct {

    @PrimaryGeneratedColumn()
    id? : number

    @Column()
    name? : string

    @ManyToOne(type => EProductType, productType => productType.id)
    product_type? : EProductType

    @OneToOne(type =>  EProductSize)
    @JoinColumn()
    size? : EProductSize

    @CreateDateColumn()
    created_at? : Date

    @UpdateDateColumn()
    updated_at? : Date
}