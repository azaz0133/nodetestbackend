import { IRequest, IResponse, INext } from 'src/interfaces/express';
import jwt from 'jsonwebtoken'


export function authorize(req: IRequest, res: IResponse, next: INext) {
    const { authorization } = req.headers
    try {
        verifyToken(authorization)
        next()
    } catch (error) {
        res.status(400).json({
            status: "unauthorized",
            message: "pls login first"
        })
    }
}

export function verifyToken(token: string): {
    userId: number,
    username: string
} {
    return jwt.verify((token as string).split(" ")[1], process.env.SECRET_KEY || "test") as {
        userId: number,
        username: string
    }
}