
import { MUser } from './model';
import { IRequest } from '../../interfaces/express';
import { IResponse } from '../../interfaces/express';


export class CUser {

    model: MUser

    constructor(){
        this.model = new MUser()
    }

    public getRegister = async (
        req:IRequest,
        res:IResponse
    ):Promise<void> => {
        try {
            
            await this.model.register(req.body)

            res.status(201).json({
                status:"created",
                message: `register user username ${req.body.username} successfully`
            })


        } catch (error) {
            res.status(500).json({
                status:"failure",
                message: error.message
            })
        }
    }

}